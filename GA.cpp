#include <iostream>
#include <stdlib.h> 
#include <cstdlib>
#include <ctime>
#include <math.h>

using namespace std;

 const int m = 7 ; //First Population Matrix Rows (Chromosomes)
 const int n = 4 ;//Genes of Chromosome (3 Gene's + 1 Fitness col in this case)
 const int max_generations = 150; 
 const int max_possible_sum = 4;
 const int max_possible_fitness = 100;
 
 
//initialzing The Prototype Population...
void  init(int matrix[][n]){
    for(int i = 0 ; i < m ; i++){
        for(int j = 0 ; j < n ; j++){
        	matrix[i][j]  = 0;            
        }
	}
}

//Calculate the Fitness Of each Chromosome and set it to nth index of it
void  setFitness   (int matrix[][n]){   
    for(int i = 0 ; i < m ; i++){
        int sum  = matrix[i][0] + matrix[i][1]*3 + matrix[i][2]*2;
        matrix[i][n-1] = max_possible_fitness - (3*abs(sum - max_possible_sum));
	}
}

//Select a Chromosome (more Fitness , more Chance to be selected)
int  doSelection   (int matrix[m][n]){
    int selected = 0;
    int sum = 0 ;
    int s = 0 ;
    for(int i = 0 ; i<m ;i++) sum+= matrix[i][n];
    int value = rand() % sum +1 ;
    for(int i = 0 ; i<m ;i++){
        s = s + matrix[i][n] ;
        if(value < s){
            selected = i ;
            break ;
        }
    } 
    return selected ;
}

//Perform The Crossover Operation
void  doCrossover  (int matrix[][n]){
    
    for(int i = 1 ; i <= m/3 ; i++){
        int temp[n-1];
        int a = rand() % m;
        int b = rand() % m;
        int k = rand()% (n-1) ;
        for(int j1 = k ; j1 < n-1 ;j1++) temp[j1] = matrix[a][j1] ;
        
        for(int x1 = k ; x1 < n-1 ;x1++) matrix[a][x1] = matrix[b][x1] ;

        for(int c = k ; c < n-1 ;c++) matrix[b][c] = temp[c] ;

    }
}

//Perform The Mutation Operation on single Random Gene of Random Chromosome 
void  doMutation   (int matrix[][n]){
    for(int i = 1 ; i <= m/3 ; i++)
    {
        int a  = rand () % m ;
        int k1 = rand()% (n-2) ;
        matrix[a][k1] = rand()% 2 ;
    }
}

//Returns Index Of the Row of the Population which has  lowest Fitness
int   getMinFitRow (int matrix[][n]){
    
    int low = matrix[0][n-1];
    int lowIndex = 0;
    for(int v =1;v<m;v++)
    {
        if(matrix[v][n-1] < low)
        {
            low = matrix[v][n-1];
            lowIndex = v;
        }
    }
    return lowIndex ;   
}

//Returns Index Of the Row of the Population which has Highest Fitness
int   getMaxFitRow (int matrix[][n]){
    
    int top = matrix[0][n-1];
    int topIndex = 0 ;

    for(int v =1;v<m;v++){
        if(matrix[v][n-1] > top) {
            top= matrix[v][n-1];
            topIndex = v;
        }
    }
    return topIndex ;
}
//Print Matrix
void  printMatrix  (int matrix[][n] ){   
    cout <<endl ;
    for(int i = 0 ; i < m ; i++)
    {
        for(int j = 0 ; j <= n ; j++)    
        {
        	cout <<matrix[i][j] << "  ";             
        }
        cout << "\n" ;
	}
}

int main()
{	
	// Initialize random number generator 
	srand(time(0));
	cout << "\n Number of Nodes : " << n  << "\n" ;
	
	//A new Random Population.
    int population[m][n];
    init(population);
    
    // Calculate fitness
    setFitness(population);
    cout << endl << "The initial population " << endl ;
    printMatrix(population);
    
    //Generating Next Generations...
    for(int p = 0 ; p <max_generations ; p++){
        int temp[n];
        
        //Copy best Fitness row in a temp array 
        for(int j=0 ; j<n ; j++)
            temp[j] = population[getMaxFitRow(population)][j]; 
            
        cout <<endl << ">>> " <<p+1 << "th Generation " ;
        
        doCrossover(population);
        doMutation(population);
        setFitness(population);
        
        //Replacing Worst Fitnessed with best row
        for(int j=0; j<n ;j++)
            population[getMinFitRow(population)][j] = temp[j]; 
       
        printMatrix(population);
        
        // check if finished aand all have maximum fitness
        bool finished = true;
        for (int i=0; i<m; i++){
        	if(population[i][n-1] != max_possible_fitness){
        		finished = false;
        		break;
			}
		}
		if (finished) break;
    }
    cout << "\n--------------------\n";
    int sum =  population[0][0] +  population[0][1] *4 + population[0][2] *3;
    cout << "Maximum value for x1 + 4x2 + 3x3 is : "  << sum << endl;
    return 0;
	

}

